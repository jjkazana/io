"""
@author: jjkazana
"""
from .DataStruct.Database import Database
import os

class Comparator(object):

	def __init__(self):
		#lists of:
		self.Results = []
		self.Errors = []
		self.Databases = []

	def AddDatabase(self,loader, path):
		self.Databases.append( Database(path, loader) )

	#saves results to file (CSV for ease?)
	#def Save(values_to_be_saved, filename_under_which_they_are_to_be_saved):
		#no idea for now on how to do this
		

	def Run(self):
		results = []
		for database in self.Databases:
			for Procedure in database.Procedures:
				if(Procedure._canRunOn(database.DataFields())):
					for patient in Procedure.patients:
						for data in database.load(os.path.join(database.path, patient)):
							try:
								results.append(Procedure.resultsFor(data))
							except Exception as e:
								print(e)
								self.Errors.append(e)
				else:
					self.Errors.append(Procedure.AttributesError())				
		self.Results = results



cmp.Run()

